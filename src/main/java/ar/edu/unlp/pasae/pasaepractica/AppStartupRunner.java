package ar.edu.unlp.pasae.pasaepractica;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import ar.edu.unlp.pasae.pasaepractica.entity.Person;
import ar.edu.unlp.pasae.pasaepractica.repositories.IPersonRepository;

@Component
public class AppStartupRunner implements ApplicationRunner {

	@Autowired
	private IPersonRepository personRepository;

	private IPersonRepository getPersonRepository() {
		return personRepository;
	}

	@Override
	public void run(final ApplicationArguments args) throws Exception {
		getPersonRepository().save(new Person("Mat�as", "Beccaria"));
		getPersonRepository().save(new Person("Mat�as", "Butti"));

	}

}