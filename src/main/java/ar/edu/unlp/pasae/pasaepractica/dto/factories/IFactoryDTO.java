package ar.edu.unlp.pasae.pasaepractica.dto.factories;

import java.util.Collection;
import java.util.List;

import ar.edu.unlp.pasae.pasaepractica.dto.AutoDTO;
import ar.edu.unlp.pasae.pasaepractica.dto.PersonDTO;
import ar.edu.unlp.pasae.pasaepractica.entity.Auto;
import ar.edu.unlp.pasae.pasaepractica.entity.Person;

/**
 * Clase que representa a la interfaz de la fábrica de DTO's
 *
 * @author mbecca
 *
 */
public interface IFactoryDTO {

	AutoDTO convertToAutoDTO(Auto auto);

	Collection<AutoDTO> convertToAutoDTOs(List<Auto> patentes);

	/**
	 * Convierte un objeto Persona en su correspondiente representación con DTO
	 *
	 * @author mbecca
	 *
	 */
	PersonDTO convertToPersonDTO(Person aPerson);

	/**
	 * Convierte una colección de objetos Persona en su correspondiente
	 * representación con DTO's
	 *
	 * @author mbecca
	 *
	 */
	Collection<PersonDTO> convertToPersonDTOs(List<Person> persons);

}
