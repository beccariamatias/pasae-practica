package ar.edu.unlp.pasae.pasaepractica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 * Clase para inicializar la aplicación con Spring Boot
 *
 * @author mbecca
 *
 */
@SpringBootApplication
@EnableElasticsearchRepositories(basePackages = "ar.edu.unlp.pasae.pasaepractica.repositories")
public class PasaePracticaApplication {

	public static void main(final String[] args) {
		SpringApplication.run(PasaePracticaApplication.class, args);
	}

}
