package ar.edu.unlp.pasae.pasaepractica.services.api;

import java.util.Collection;

import ar.edu.unlp.pasae.pasaepractica.dto.AutoDTO;

public interface IAutoService {

	Collection<AutoDTO> findByPatente(String patente);

}
