package ar.edu.unlp.pasae.pasaepractica.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import ar.edu.unlp.pasae.pasaepractica.validations.annotations.PhoneNumberConstraint;

public class PhoneNumberValidator implements ConstraintValidator<PhoneNumberConstraint, String> {

	@Override
	public boolean isValid(final String phoneNumber, final ConstraintValidatorContext cxt) {
		// No lo consideramos un campo obligatorio
		if (phoneNumber == null || phoneNumber.isEmpty())
			return true;

		return phoneNumber.matches("[0-9]+") && phoneNumber.length() > 7 && phoneNumber.length() < 14;
	}
}
